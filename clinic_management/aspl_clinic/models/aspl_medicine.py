# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class AsplMedicine(models.Model):
    _name = 'aspl.medicine'
    _description ='medicine information'
    _rec_name = 'name'

    name=fields.Char(string='Name')
    product_id = fields.Many2one('product.product', string='Product')
    medicine_id= fields.Many2one('aspl.visit', string='Medicine')
    price=fields.Integer(string='Price',default=5)
    qty=fields.Float(string='Qty')
    subtotal = fields.Integer(string='Subtotal', compute='_compute_total',store =True)

    @api.depends('price', 'qty')
    def _compute_total(self):
        for record in self:
            record.subtotal = record.price * record.qty

    @api.constrains('price')
    def _check_something(self):
        for record in self:
            if record.price < 0:
                raise ValidationError("Price cannot be negative")

    def search_confirm(self):
        sessions = self.search([('id','=',111)],limit=5,offset=0,count=True)
        print('\n\n\n\n\n\n\n\n\nsessions',sessions)
        max_sequence = self.env['id.card'].search_read([('name','=','ravi')])
        print('\n\n\n\n\n\n\ntotal number of fields and value', max_sequence)
        max_value = self.env['id.card'].search_count([('id','=','3')])
        print('\n\n\n\n\n\n\ntotal number of recoed', max_value)
        medicine = self.env['aspl.appointment'].browse([10,14,4,5,6,55,44,])
        print('\n\n\n\n\n\n\n\nbrowsemethod', medicine)

        group= self.env.ref('aspl_clinic.aspl_clinic_from')
        print('\n\n\n\n\n\n\ngroup',group)
        #
        # std_obj = self.read(fields)
        # print('\n\n\n\n\n\n\n\n\n\nstd_obj',std_obj)

    @api.model
    def default_get(self, fields):
        res = super(AsplMedicine, self).default_get(fields)
        res.update({'price':10})
        print('\n\n\n\n\n\n\n\n\ndefault', res)
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=True, submenu=True):
        print('\n\n---->call')
        print("\n\n\n\n\n\n\n\n------------viewid",view_id)
        res = super(AsplMedicine, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        print('\n\n\n\n\n\n\n\n\nfields_id',res)
        return res

    @api.model
    def create(self,vals):
        record=super(AsplMedicine,self).create(vals)
        record.qty=5
        print('\n\n\n\n\n\n\n\n\n\n', vals)
        print('\n\n\n\n\n\n\n\n\n\n\n', record)
        return record

    def write(self, vals):
        recod=super(AsplMedicine,self).write(vals)
        print('\n\n\n\n\n\n\n\n\nvals', vals)
        print('\n\n\n\n\n\n\n\nrecoed', recod)
        return recod

    def unlink(self):
        res = super(AsplMedicine, self).unlink()
        print("\n\n\n\n\n\n\nresunlink",res)
        return res

    def copy(self):
        data = super(AsplMedicine, self).copy()
        print('\n\n\n\n\n\n\n\n\ncopymethod',data)
        return data

    def action_url(self):
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'https://acespritech.com/',
        }