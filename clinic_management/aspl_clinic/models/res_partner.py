# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models,fields


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    is_patient = fields.Boolean(string='Is Patient')
    is_doctor = fields.Boolean(string='Is Doctor')

    def name_get(self):
        name_get = []
        for res in self:
            if res.name:
                name_get.append((res.id, '%s %s' % (res.name, res.parent_id.name)))
                print('\n\n\n\n\n\n\n\n\n',name_get)
        return name_get
    #
    # @api.model
    # def name_search(self, name='', args=None, operator='ilike', limit=100):
    #     ids = self._name_search(name, args, operator, limit=limit)
    #     print('\n\n\n\n\n\n\n\n--------------->',ids)
    #     return ids