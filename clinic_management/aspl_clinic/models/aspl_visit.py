# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
import base64
import xlrd
from odoo import models, fields, api
import datetime
from xlwt import Workbook


class AsplVisit(models.Model):
    _name = 'aspl.visit'
    _description = 'patient visit information'
    _rec_name = 'name'

    state = fields.Selection(
        [('default', 'draft'), ('waiting', 'Waiting'), ('progress', 'In Progress'), ('complete', 'Complete'),
         ('cancel', 'Cancel')], default='default')
    sequence = fields.Char(string="Service Number", readonly=True, default='New')
    image = fields.Binary(string='Image')
    date = fields.Datetime(string='Date')
    name = fields.Char(string='Name')
    patient_id = fields.Many2one('res.partner', string='Patient')
    start_time = fields.Datetime(string="Start Time")
    complete_time = fields.Datetime(string='Complete Time')
    dob = fields.Date(string='Date of Birth')
    doctor = fields.Many2one('res.partner', string='Doctor')
    notes = fields.Text(string='Notes', copy=False)
    add_info = fields.Char(string='other inform')
    medicine_lines = fields.One2many('aspl.medicine', 'medicine_id', string='Medicine Line')
    allergies = fields.Many2many('aspl.allergy', string='Allergies')
    identities_id = fields.One2many('card.line', 'identities_ids', string='Identities Id')
    total = fields.Monetary(string='Total', currency_field='currency_id', compute='_compute_total')
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  default=lambda self: self.env.company.currency_id.id, store=True)
    user_id = fields.Many2one('res.users', string='user')
    smart = fields.Integer(string='Smart')
    report_template = fields.Binary(string='Report')

    def print_report(self):
        return self.env.ref('aspl_clinic.action_report_visit').report_action(self)

    @api.model
    def create(self, vals):
        vals['sequence'] = self.env['ir.sequence'].next_by_code('aspl.visit')
        return super(AsplVisit, self).create(vals)

    _sql_constraints = [
        ('name', ' unique(name)', 'name is already exists!')
    ]

    def arrived(self):
        self.state = 'waiting'

    def start(self):
        self.state = 'progress'
        self.date = datetime.datetime.now()

    def complete(self):
        self.state = 'complete'

    def cancel(self):
        self.state = 'cancel'

    @api.depends('medicine_lines.subtotal')
    def _compute_total(self):
        self.total = sum(self.medicine_lines.mapped('subtotal'))

    def report_xls(self):
        wb = Workbook()
        sheet1 = wb.add_sheet('sheet 1')

        sheet1.write(0, 0, 'Name')
        sheet1.write(0, 1, 'Patient')
        sheet1.write(0, 2, 'Start Time')
        sheet1.write(0, 3, 'Complete Time')
        sheet1.write(0, 4, 'Dob')
        sheet1.write(0, 5, 'Doctor')
        sheet1.write(0, 6, 'notes')

        sheet1.write(1, 0, self.name)
        sheet1.write(1, 1, self.patient_id.name)
        sheet1.write(1, 2, str(self.start_time))
        sheet1.write(1, 3, str(self.complete_time))
        sheet1.write(1, 4, str(self.dob))
        sheet1.write(1, 5, self.doctor.name)
        sheet1.write(1, 6, self.notes)

        wb.save('example.xlsx')

        path = '/home/acespritech/workspace/odoo_14/odoo/example.xlsx'
        wb = xlrd.open_workbook(path)
        sheet = wb.sheet_by_index(0)

        print(sheet.cell_value(1, 0))
        print(sheet.cell_value(1, 1))
        print(sheet.cell_value(1, 2))
        print(sheet.cell_value(1, 3))
        print(sheet.cell_value(1, 4))
        print(sheet.cell_value(1, 5))

        self.report_template = base64.b64encode(
            open("/home/acespritech/workspace/odoo_14/odoo/example.xlsx", "rb").read())
        return {
            'type': 'ir.actions.act_url',
            'name': 'contract',
            'url': '/web/content/aspl.visit/%s/report_template/example.xlsx?download=true' % (self.id),
        }
