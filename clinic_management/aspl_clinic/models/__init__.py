# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################


from . import  aspl_medicine
from . import  aspl_allergy
from . import aspl_id_cards
from . import  aspl_id_cards_line
from . import aspl_appointment
from . import aspl_visit
from . import res_partner

