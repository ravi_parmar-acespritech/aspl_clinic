# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
from odoo import models,fields,api

from datetime import date, datetime
from dateutil.relativedelta import relativedelta


class AsplAppointment(models.Model):
    _name = 'aspl.appointment'
    _description='patient appointment information'

    state = fields.Selection([('default', 'Draft'), ('confirm', 'Confirm'),('done', 'Done')],default='default')
    sequence = fields.Char(string="Service Number", readonly=True, default='New')
    name = fields.Char(string='Name')
    image = fields.Binary(string='Image')
    patient_id = fields.Many2one('res.partner',string='Patient')
    birthday = fields.Date(string='Date of Birth')
    age = fields.Integer(string="Age")
    mobile = fields.Char(string='Mobile')

    @api.model
    def create(self, vals):
        vals['sequence'] = self.env['ir.sequence'].next_by_code('aspl.appointment')
        return super(AsplAppointment, self).create(vals)

    def appointment_confirm(self):
        self.state = 'confirm'

    def appointment_done(self):
        self.state = 'done'

    @api.onchange('birthday')
    def _onchange_birth_date(self):

        if self.birthday:
            d1 = datetime.strptime(str(self.birthday), "%Y-%m-%d").date()

            d2 = date.today()

            self.age = relativedelta(d2, d1).years

    def aspl_clinic(self):
        clinic = self.env.ref('aspl_clinic.clinic_wizard_act_views').read()[0]
        return clinic
