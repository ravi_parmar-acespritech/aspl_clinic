# -*- coding: utf-8 -*-
#################################################################################
# Author : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models,fields


class AsplCardsLine(models.Model):
    _name = 'card.line'
    _description = 'card for patient'
    _rec_name = 'name'

    name=fields.Char(string='Name')
    id_card=fields.Many2one('id.card',string='Id Cards')
    code=fields.Char(string='Code')
    identities_ids= fields.Many2one('aspl.visit',string='Identities Ids')