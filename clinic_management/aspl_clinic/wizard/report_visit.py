# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################


from odoo import models, fields
from xlwt import Workbook
import base64


class ReportVisit(models.TransientModel):
    _name = 'report.visit'
    _description = 'visit delay report'

    visit_id = fields.Many2many('aspl.visit', string='Patient Name')
    report_selected = fields.Selection([('type1', 'PDF Report'), ('type2', 'Excel Report')])
    report_template = fields.Binary(string='Report')

    def patient_visit_report(self):
        if self.report_selected == 'type1':
            return self.env.ref('aspl_clinic.action_report_visit').report_action(self.visit_id)
        else:
            wb = Workbook()
            sheet1 = wb.add_sheet('sheet 1')

            sheet1.write(0, 0, 'Name')
            sheet1.write(0, 1, 'Patient')
            sheet1.write(0, 2, 'Start Time')
            sheet1.write(0, 3, 'Complete Time')
            sheet1.write(0, 4, 'Dob')
            sheet1.write(0, 5, 'Doctor')
            sheet1.write(0, 6, 'notes')
            i = 1
            for value in self.visit_id:
                sheet1.write(i, 0, value.name)
                sheet1.write(i, 1, value.patient_id.name)
                sheet1.write(i, 2, str(value.start_time))
                sheet1.write(i, 3, str(value.complete_time))
                sheet1.write(i, 4, str(value.dob))
                sheet1.write(i, 5, value.doctor.name)
                sheet1.write(i, 6, value.notes)
                i = i + 1

            wb.save('example.xlsx')

            self.report_template = base64.b64encode(
                open("/home/acespritech/workspace/odoo_14/odoo/example.xlsx", "rb").read())
            return {
                'type': 'ir.actions.act_url',
                'name': 'contract',
                'url': '/web/content/report.visit/%s/report_template/example.xlsx?download=true' % (self.id),
            }


