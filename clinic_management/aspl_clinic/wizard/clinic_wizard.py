# -*- coding: utf-8 -*-
#################################################################################
# Author: Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import fields, models


class ClinicWizard(models.TransientModel):
    _name = "clinic.wizard"
    _description = "clinic wizard information"

    name = fields.Char(string='Name')
    reason = fields.Text(string='Reason')

    def data_save(self):
        print('\n\n\n\n\n\n\n\n---------> patient health problems')
