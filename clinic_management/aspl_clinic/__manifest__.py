# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Aspl Clinic',
    'version': '14.1.1',
    'summary': 'clinic systems module',
    'sequence': 5,
    'description': 'Clinic, an organized medical service offering diagnostic',
    'category': 'clinic systems module',
    'website': 'https://acespritech.com/',
    'depends':['mail','web','sale_management','base','stock'],
    'data':[
            'security/ir.model.access.csv',
            'wizard/clinic_wizard.xml',
            'wizard/visit_report.xml',
            'wizard/report_visit.xml',
            'data/appointment_data.xml',
            'data/clinic_data.xml',
            'report/report.xml',
            'report/visit_report_template.xml',
            'report/medicine_report_template.xml',
            'views/aspl_medicine.xml',
            'views/aspl_allergy.xml',
            'views/aspl_id_card.xml',
            'views/aspl_id_card_line.xml',
            'views/aspl_appointment.xml',
            'views/aspl_visit_views.xml',
            'views/res_partner_view.xml',

    ],
    'author': 'ravi',
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'OPL-1',

}